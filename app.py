import random
from flask import Flask, request, jsonify
from model import Model

app = Flask(__name__)
model = Model()

@app.route('/', methods=['GET'])
def endpoint():
    x = random.uniform(1, 10)
    y = model.predict(x)
    return jsonify({'x': x, 'y': y})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
