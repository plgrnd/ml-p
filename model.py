import numpy as np
from sklearn.linear_model import LinearRegression

class Model:
    def __init__(self):
        house_data = np.array([
            [1.1, 2.2],
            [1.9, 3.3],
            [2.8, 4.0],
            [3.4, 4.7]
        ])
        self.X = house_data[:, 0].reshape(-1, 1)
        self.y = house_data[:, 1]
        self.model = LinearRegression()
        self.model.fit(self.X, self.y)

    def predict(self, size):
        x = np.array([[size]])
        prediction = self.model.predict(x)
        return prediction[0]
