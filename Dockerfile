FROM python:3.10.12-slim
WORKDIR /workspace/app
COPY *.py ./
RUN pip install opentelemetry-distro opentelemetry-exporter-otlp numpy flask scikit-learn && \
    opentelemetry-bootstrap -a install
EXPOSE 5000
ENTRYPOINT ["opentelemetry-instrument", "python", "app.py"]
